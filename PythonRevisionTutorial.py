#Python Revision Tutorial
import sys
import math
""" Multiple line
comment """
# if else statements
number = 23
guess = int(raw_input('Enter an integer : '))
if guess == number:
print 'Congratulations, you guessed it.' # New block starts here
print "(but you do not win any prizes!)" # New block ends here
elif guess < number:
print 'No, it is a little higher than that' # Another block
# You can do whatever you want in a block ...
else:
print 'No, it is a little lower than that'
# you must have guess > number to reach here
print 'Done'

# While Statement
running = True
while running:
    guess = int(raw_input('Enter an integer : '))
    if guess == number:
        print 'Congratulations, you guessed it.'
        running = False # this causes the while loop to stop
    elif guess < number:
        print 'No, it is a little higher than that.'
    else:
        print 'No, it is a little lower than that.'
else:
    print 'The while loop is over.'
    # Do anything else you want to do here
print 'Done'

# For Statement
for i in range(1,5):
    print i
else:
    print 'the for loop is over'

#Functions
#only those at the end of the parameter list can be given default value
def printMax(a, b):
    if a > b:
        print a, 'is maximum�
    else:
        print b, 'is maximum�
printMax(3, 4) # directly give literal values
x = 5
y = 7
printMax(x, y) # give variables as arguments

#Module
# Filename: mymodule.py
"""
def sayhi():
    print 'Hi, this is mymodule speaking.�
version = '0.1'
# End of mymodule.py
import mymodule
mymodule.sayhi()
print 'Version', mymodule.version
"""
# Dictionary
ab = { 'Alex' : 'alex@gmail.com', 'Bob' : �bob@yahoo.com�}
print "Alex's email is %s" % ab['Alex']
# Adding a key/value pair
ab['Cindy'] = 'cindy@gmail.com'
# Deleting a key/value pair
del ab['Alex']
print '\nThere are %d contacts in the address-book\n' % len(ab)
for name, address in ab.items():
    print 'Contact %s at %s' % (name, address)
if 'Cindy' in ab: # OR ab.has_key('Cindy')
    print "\nCindy's email is %s" % ab['Cindy']

