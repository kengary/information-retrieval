#!/usr/bin/python
import re
import nltk
import sys
import getopt
import string
from nltk.util import ngrams
import operator

def build_LM(in_file):
    """
    build language models for each label
    each line in in_file contains a label and an URL separated by a tab(\t)
    """
    print 'building language models...'
    #Init Dictionaries
    tamilDict = {}
    indonesianDict = {} 
    malaysianDict = {}
    #4grams
    n = 4
    """
    Open and read the file and count frequency first
    """
    try:
        fileReader = open(in_file, 'r')
        for line in fileReader:
            line = line.split(' ', 1)
            line[1] = line[1].rstrip()
            ntgrams = ngrams(list(line[1]), n, pad_left=True, pad_right=True, pad_symbol='#')
            if(line[0] == 'tamil'):
                for gram in ntgrams:
                    if(tamilDict.has_key(gram)):
                        tamilDict[gram] = tamilDict[gram]+1.0
                    else:
                        tamilDict[gram] = 1.0
            elif(line[0] == 'indonesian'):
                for gram in ntgrams:
                    if(indonesianDict.has_key(gram)):
                        indonesianDict[gram] = indonesianDict[gram]+1.0
                    else:
                        indonesianDict[gram] = 1.0
            else:
                for gram in ntgrams:
                    if(malaysianDict.has_key(gram)):
                        malaysianDict[gram] = malaysianDict[gram]+1.0
                    else:
                        malaysianDict[gram] = 1.0  
        fileReader.close()
    except IOError:
        sys.stderr.write('err reading:' + in_file)
    '''
    Build LM with probabilities
    '''
    LM = []
    totalCount = 0.0
    for gram,count in tamilDict.items():
        totalCount += count
    for gram,count in tamilDict.items():
        tamilDict[gram] = 1.0 + (count/totalCount)
    totalCount = 0.0
    for gram,count in indonesianDict.items():
        totalCount += count
    for gram,count in indonesianDict.items():
        indonesianDict[gram] = 1.0 + (count/totalCount)
    totalCount = 0.0
    for gram,count in malaysianDict.items():
        totalCount += count
    for gram,count in malaysianDict.items():
        malaysianDict[gram] = 1.0 + (count/totalCount)
    LM.append(tamilDict)
    LM.append(indonesianDict)
    LM.append(malaysianDict)
    return LM
    
def test_LM(in_file, out_file, LM):
    print "testing language models..."
    '''
    Do the same preprocess for test query
    '''
    n=4
    try:
        fileReader = open(in_file, 'r')
        fileWriter = open(out_file, 'w')
        for line in fileReader:
            orgLine = line
            line = line.rstrip()
            ntgrams = ngrams(list(line), n, pad_left=True, pad_right=True, pad_symbol='#')
            tamilP = 1;
            indonesianP = 1;
            malaysianP = 1;
            otherP = 0;
            for gram in ntgrams:
                if gram in LM[0]:
                    tamilP *= LM[0][gram]
                if gram in LM[1]:
                    indonesianP *= LM[1][gram]
                if gram in LM[2]:
                    malaysianP *= LM[2][gram]
                if gram not in LM[0] and gram not in LM[1] and gram not in LM[2]:
                    otherP += 1                  
            
            if  float(otherP)/ float(len(ntgrams)) > 0.5 :
                # more than half the grams are alien
                label = "other"
            else:
                # one of the langauge, update rebuild the LM again
                labelDict = {tamilP: "tamil", indonesianP: "indonesian", malaysianP: "malaysian"}
                label = labelDict[max(tamilP,indonesianP,malaysianP)]
                if label == "tamil":
                    for gram in ntgrams:
                        if(LM[0].has_key(gram)):
                            LM[0][gram] = LM[0][gram]+1.0
                        else:
                            LM[0][gram] = 1.0
                    totalCount = 0.0
                    for gram,count in LM[0].items():
                        totalCount += count
                    for gram,count in LM[0].items():
                        LM[0][gram] = 1.0 + (count/totalCount)
                if label == "indonesian":
                    for gram in ntgrams:
                        if(LM[1].has_key(gram)):
                            LM[1][gram] = LM[1][gram]+1.0
                        else:
                            LM[1][gram] = 1.0
                    totalCount = 0.0
                    for gram,count in LM[1].items():
                        totalCount += count
                    for gram,count in LM[1].items():
                        LM[1][gram] = 1.0 + (count/totalCount)
                if label == "malaysian":
                    for gram in ntgrams:
                        if(LM[2].has_key(gram)):
                            LM[2][gram] = LM[2][gram]+1.0
                        else:
                            LM[2][gram] = 1.0
                    totalCount = 0.0
                    for gram,count in LM[2].items():
                        totalCount += count
                    for gram,count in LM[2].items():
                        LM[2][gram] = 1.0 + (count/totalCount)    
            fileWriter.write(label+" "+orgLine)
        fileReader.close()
        fileWriter.close()
    except IOError:
        sys.stderr.write('err reading:' + in_file)
    
def usage():
    print "usage: " + sys.argv[0] + " -b input-file-for-building-LM -t input-file-for-testing-LM -o output-file"

input_file_b = input_file_t = output_file = None
try:
    opts, args = getopt.getopt(sys.argv[1:], 'b:t:o:')
except getopt.GetoptError, err:
    usage()
    sys.exit(2)
for o, a in opts:
    if o == '-b':
        input_file_b = a
    elif o == '-t':
        input_file_t = a
    elif o == '-o':
        output_file = a
    else:
        assert False, "unhandled option"
if input_file_b == None or input_file_t == None or output_file == None:
    usage()
    sys.exit(2)

LM = build_LM(input_file_b)
test_LM(input_file_t, output_file, LM)
