1) In the homework assignment, we are using character-based ngrams, i.e., the gram units are characters. Do you expect token-based ngram models to perform better?
ANS:
- Character-based ngram in my opinion should work better than token-based. 
- Character-based ngram is able to get the stem word, its variants affixes (e.g. -ed, -ing, etc), vocabulary, usage of symbols or punctuation in the language. 
- Character-based ngram has much lesser combination occurrences than token-based meaning less zero frequency statistics

2) What do you think will happen if we provided more data for each category for you to build the language models? What if we only provided more data for Indonesian?
ANS:
- My program might be slower in generating LM, might be more accurate in prediction but also prone to noisy/ sparse info if data is not properly filtered.
- The program will bias towards Indonesian, and will predict more lines from test as Indonesia

3) What do you think will happen if you strip out punctuations and/or numbers? What about converting upper case characters to lower case?
- Tried doing the following:
			#line[1]= line[1].lower()
            #line[1] = ''.join([i for i in line[1] if not i.isdigit()]) #remove numbers
            #line[1] = re.sub(r'[\x00-\x08\x0b\x0c\x0e-\x1f\x7f-\xff]', '', line[1]) #remove asci escape chars
            #line[1] = line[1].translate(string.maketrans("",""), string.punctuation) #remove punctuation          
            #line[1] = " ".join(line[1].split()) #remove extra spaces
- Result was worse for all individual cases compared to not doing them.

4) We use 4-gram models in this homework assignment. What do you think will happen if we varied the ngram size, such as using unigrams, bigrams and trigrams?
- A combination of varying ngrams e.g. bigram + trigram might be more accurate in prediction than just 4-gram.