#!/usr/bin/python
import re
import nltk
import sys
import getopt
import string
from nltk.util import ngrams
import operator

def build_LM(in_file):
    """
    build language models for each label
    each line in in_file contains a label and an URL separated by a tab(\t)
    """
    print 'building language models...'
    # This is an empty method
    # Pls implement your code in below
    """
    Open and read the file
    """
    tamilDict = {}
    indonesianDict = {} 
    malaysianDict = {}
    n = 4
    try:
        fileReader = open(in_file, 'r')
        for line in fileReader:
            line = line.split(' ', 1)
            line[1] = line[1].rstrip()
            ntgrams = ngrams(list(line[1]), n, pad_left=True, pad_right=True, pad_symbol='#')
            if(line[0] == 'tamil'):
                for gram in ntgrams:
                    if(tamilDict.has_key(gram)):
                        tamilDict[gram] = tamilDict[gram]+1.0
                    else:
                        tamilDict[gram] = 1.0
            elif(line[0] == 'indonesian'):
                for gram in ntgrams:
                    if(indonesianDict.has_key(gram)):
                        indonesianDict[gram] = indonesianDict[gram]+1.0
                    else:
                        indonesianDict[gram] = 1.0
            else:
                for gram in ntgrams:
                    if(malaysianDict.has_key(gram)):
                        malaysianDict[gram] = malaysianDict[gram]+1.0
                    else:
                        malaysianDict[gram] = 1.0  
        fileReader.close()
    except IOError:
        sys.stderr.write('err reading:' + in_file)
    LM = []
    totalCount = 0.0
    for gram,count in tamilDict.items():
        totalCount += count
    for gram,count in tamilDict.items():
        tamilDict[gram] = 1.0 + (count/totalCount)
    totalCount = 0.0
    for gram,count in indonesianDict.items():
        totalCount += count
    for gram,count in indonesianDict.items():
        indonesianDict[gram] = 1.0 + (count/totalCount)
    totalCount = 0.0
    for gram,count in malaysianDict.items():
        totalCount += count
    for gram,count in malaysianDict.items():
        malaysianDict[gram] = 1.0 + (count/totalCount)
    LM.append(tamilDict)
    LM.append(indonesianDict)
    LM.append(malaysianDict)
    return LM
    
def test_LM(in_file, out_file, LM):
    print "testing language models..."
    # This is an empty method
    # Pls implement your code in below
    n=4
    try:
        fileReader = open(in_file, 'r')
        fileWriter = open(out_file, 'w')
        for line in fileReader:
            orgLine = line
            line = line.rstrip()
            ntgrams = ngrams(list(line), n, pad_left=True, pad_right=True, pad_symbol='#')
            tamilP = 1;
            indonesianP = 1;
            malaysianP = 1;
            otherP = 0;
            for gram in ntgrams:
                if gram in LM[0]:
                    tamilP *= LM[0][gram]
                if gram in LM[1]:
                    indonesianP *= LM[1][gram]
                if gram in LM[2]:
                    malaysianP *= LM[2][gram]
                if gram not in LM[0] and gram not in LM[1] and gram not in LM[2]:
                    otherP += 1                  
            if  float(otherP)/ float(len(ntgrams)) > 0.5 :
                label = "other"
            else:
                labelDict = {tamilP: "tamil", indonesianP: "indonesian", malaysianP: "malaysian"}
                label = labelDict[max(tamilP,indonesianP,malaysianP)]
            fileWriter.write(label+" "+orgLine)
        fileReader.close()
        fileWriter.close()
    except IOError:
        sys.stderr.write('err reading:' + in_file)
    
def usage():
    print "usage: " + sys.argv[0] + " -b input-file-for-building-LM -t input-file-for-testing-LM -o output-file"

input_file_b = input_file_t = output_file = None
try:
    opts, args = getopt.getopt(sys.argv[1:], 'b:t:o:')
except getopt.GetoptError, err:
    usage()
    sys.exit(2)
for o, a in opts:
    if o == '-b':
        input_file_b = a
    elif o == '-t':
        input_file_t = a
    elif o == '-o':
        output_file = a
    else:
        assert False, "unhandled option"
if input_file_b == None or input_file_t == None or output_file == None:
    usage()
    sys.exit(2)

LM = build_LM(input_file_b)
test_LM(input_file_t, output_file, LM)
