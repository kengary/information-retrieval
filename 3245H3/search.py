#!/usr/bin/python
import getopt
import sys
import string
import nltk
import heapq
import math
import linecache

porter = nltk.PorterStemmer()

def ltcincForDocument(termPostingPair,dict,queryTerm):
    N = len(dict)
    tf = float(termPostingPair[1])
    if  tf > 0:
        dtf = 1.0 + math.log(tf,10)
    else:
        dtf= 0
    idf = 1.0 + math.log(N/float(dict[queryTerm][0]),10)
    dtfidf = dtf * idf
    return dtfidf

def tfForQueryTerm(queryTerm,query):
    qtf = 1 + math.log(float(query.count(queryTerm)),10)
    return qtf

def fetchPostingList(dictInfo, file_p):
    retrieved_line = linecache.getline(file_p, int(dictInfo[1])).rstrip()
    tokens = retrieved_line.split()
    newlist = []
    while(tokens):
        a = tokens.pop()
        b = tokens.pop()
        newlist.append((b,a))
    return newlist # (docID1, tf1)

def ranking(query, dict, file_p):
    N = len(dict)
    topTen = []
    Scores = {}
    Length = {}
    for queryTerm in query:
        if queryTerm in dict:
            wtq = tfForQueryTerm(queryTerm,query)
            termPostingPairList = fetchPostingList(dict[queryTerm], file_p)
            length = 0 
            for termPostingPair in termPostingPairList:
                wtd = ltcincForDocument(termPostingPair,dict,queryTerm)
                if termPostingPair[0] in Scores:
                    Scores[termPostingPair[0]] += (wtq * wtd)
                else:
                    Scores[termPostingPair[0]] = (wtq * wtd)
                length += wtd**2
                Length[termPostingPair[0]] = 1.0/ math.sqrt(length)
    h = {}
    for d in Length:
        Scores[d] = (Scores[d] / Length[d]) * -1
        h[Scores[d]] = d
        if len(topTen) < 10:
            heapq.heappush(topTen, Scores[d])
        else:
            heapq.heappushpop(topTen, Scores[d])
    rs = []
    for t in topTen:
        rs.append(h[t])
    return rs

def getDictionary(file_d):
    dict = {}
    dictFileReader = open(file_d, 'r')
    for line in dictFileReader:
        tokens = nltk.word_tokenize(line.translate(None, string.punctuation))
        dict[tokens[0]] = (tokens[1],tokens[2])
    return dict

def parseQuery(line):
    tokens = nltk.word_tokenize(line.translate(None, string.punctuation))
    queryList = [porter.stem(token.lower()) for token in tokens if len(token)>3]
    return queryList

def main(file_d,file_p,file_q,file_o):
    dict = getDictionary(file_d)
    resultset = []
    queryFileReader = open(file_q, 'r')
    for line in queryFileReader:
        query = parseQuery(line)
        resultset.append(ranking(query, dict, file_p))
    fileWriter = open(file_o, 'w')
    for s in resultset:
        v =  s[0], ' '.join(map(str, s[1:]))
        fileWriter.write(' '.join(v))
        fileWriter.write('\n')
    fileWriter.close()
    return True
    
def usage():
    print "usage: " + sys.argv[0] + " -d dictionary-file -p postings-file -q file-of-queries -o output-file-of-results"

file_d = file_p = file_q = file_o = None
try:
    opts, args = getopt.getopt(sys.argv[1:], 'd:p:q:o:')
except getopt.GetoptError, err:
    usage()
    sys.exit(2)
for o, a in opts:
    if o == '-d':
        file_d = a
    elif o == '-p':
        file_p = a
    elif o == '-q':
        file_q = a
    elif o == '-o':
        file_o = a
    else:
        assert False, "unhandled option"
if file_d == None or file_p == None or file_q == None or file_o == None:
    usage()
    sys.exit(2)

main(file_d,file_p,file_q,file_o)
