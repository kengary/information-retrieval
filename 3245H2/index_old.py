#!/usr/bin/python
import re
import nltk
import sys
import getopt
import glob
import codecs
import string
import unicodedata
import os
from nltk import word_tokenize
from nltk.corpus import stopwords

'''
Progress: DocID and keying in frequency
'''

class InvertedIndexBuilder:
    def __init__(self, dictionaryPathName, postingsPathName):
        self.dictionaryPathName = dictionaryPathName
        self.postingsPathName = postingsPathName
        self.docPathName = ""
        self.tbl = dict.fromkeys(i for i in xrange(sys.maxunicode) if unicodedata.category(unichr(i)).startswith('P')) #punctuation removal
        self.tokens = []
        
    def __del__(self):
        print "Index Done"
        return True
    
    def textProcessLine(self, line):
        #remove punctuation
        line = line.translate(string.maketrans("",""), string.punctuation) #remove punctuation
        #Remove additional white spaces
        line = " ".join(line.split())
        #trim
        line = line.strip('\'"')
        return line
    
    def tokenizeOneLine(self, textProcessedLine):
        print "Tokenizing Line"
        self.tokens.extend(textProcessedLine.split());
        return True
    
    def lingusticPreProcessOneDoc(self):
        print "Pre-Process the Docs"
        porter = nltk.PorterStemmer()
        #lancaster = nltk.LancasterStemmer()
        self.tokens = [porter.stem(t) for t in self.tokens]
        wnl = nltk.WordNetLemmatize
        self.tokens = [wnl.lemmatize(t).lower() for t in self.tokens]
        stop = stopwords.words("english")
        self.tokens = [word for word in self.tokens if word not in stop]
        return True

    def processOneDocContent(self, file):
        print "Reading Docs content"
        for line in open(file, 'r'):
            line = self.textProcessLine(line)
            self.tokenizeOneLine(line)
        return True
    
    def buildDictionaryAndPostings(self, linguisticProcessedTokens, docID):
        
        return True
    
    def buildInvertedIndex(self, docsPath):
        print "Build Inverted Index - Dictionary"
        print "Build Inverted Index - Postings"
        print "Input: All Docs"
        print "Output: Inverted Index + Skip Pointers"
        
        self.docPathName = docsPath
        print self.docPathName
        files = glob.glob(docsPath)
        for singleFile in files:
            singleFilePath= os.path.splitext(singleFile)[0]
            docID = os.path.basename(singleFilePath)
            docTokens = self.processOneDocContent(singleFile)
            self.lingusticPreProcessOneDoc()
            self.buildDictionaryAndPostings(docTokens, docID)
        return True

'''
Definitions:
directory-of-documents : Stores documents to be indexed
dictionary-file : Stores the Dictionary
postings-file : Stores the Postings
e.g. : $ python index.py -i C:\Python27\nltk_data\corpora\reuters\training -d dictionary.txt -p postings.txt

Guides:

 - NLTK Tokenizers : nltk.sent_tokenize(), nltk.word_tokenize
 - NLKT Porter Stemmer : nlkt.stem.porter
 - case folding to reduce all words to lower case
 - Skip pointers in the postings list : math.sqrt(len(posting)) evenly placed on the posting list

Process:

Build inverted index (stemmin, token)

============INDEX.PY===========

Dictionary
==========

Posting
==========

Skip Pointer DS
===============

Implement disk-based indexing
=============================


=============SEARCH.PY==========
print file
read from file (retrieval using skip pointer file)
Boolean Retrieval
'''
    
def build_InvertedIndex(file_d, file_p):
    IB = InvertedIndexBuilder (file_d, file_p)
    IB.buildInvertedIndex(file_i)

def usage():
    print "usage: " + sys.argv[0] + " -i directory-of-documents -d dictionary-file -p postings-file"

file_i = file_d = file_p = None
try:
    opts, args = getopt.getopt(sys.argv[1:], 'i:d:p:')
except getopt.GetoptError, err:
    usage()
    sys.exit(2)
for o, a in opts:
    if o == '-i':
        file_i = a
    elif o == '-d':
        file_d = a
    elif o == '-p':
        file_p = a
    else:
        assert False, "unhandled option"
if file_i == None or file_d == None or file_p == None:
    usage()
    sys.exit(2)

II = build_InvertedIndex(file_d,file_p)
