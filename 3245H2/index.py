import os
import getopt
import glob
import sys
import string
import nltk
from nltk.corpus import stopwords
import operator

'''
 PARSE A DOC INTO LIST OF WORDS
'''
def tokeniseDoc(doc):
    s = open(doc).read()
    tokens = nltk.word_tokenize(s.translate(None, string.punctuation))
    stop = stopwords.words("english")
    porter = nltk.PorterStemmer()
    wnl = nltk.WordNetLemmatizer()
    cleanup = [wnl.lemmatize(porter.stem(token.lower())) for token in tokens if token not in stop and len(token)>2]
    return cleanup

def extendCurrentTokens(tokenSet, tokenisedDoc, docID):
    tokenisedDoc = list(set(tokenisedDoc))
    for token in tokenisedDoc:
        tokenSet.append((token, int(docID)))
    return tokenSet

def buildInvertedIndex(invertedIndex,tokenSet):
    tokenSet.sort(key=operator.itemgetter(0,1))
    for token in tokenSet:
        if invertedIndex.has_key(token[0]):
            newDocFreq = invertedIndex[token[0]][0] + 1
            invertedIndex[token[0]][1].append(token[1])
            newDocList = invertedIndex[token[0]][1]
            invertedIndex[token[0]] = [newDocFreq, newDocList]
        else:
            n = [token[1]]
            invertedIndex[token[0]] = (1,n)
    return invertedIndex

def main(file_i,file_d,file_p):
    files = glob.glob(file_i)
    tokenSet= []
    invertedIndex = {}
    invertedIndexSorted = []
    for singleFile in files:
        singleFilePath = os.path.splitext(singleFile)[0]
        docID = os.path.basename(singleFilePath)
        tokenisedDoc = tokeniseDoc(singleFile)
        tokenSet = extendCurrentTokens(tokenSet, tokenisedDoc, docID)
    invertedIndex = buildInvertedIndex(invertedIndex,tokenSet)
    for key in sorted(invertedIndex.iterkeys()):
        invertedIndexSorted.append(((key,invertedIndex[key][0]), invertedIndex[key][1]))
    fileWriterD = open(file_d, 'w')
    fileWriterP = open(file_p, 'w')
    for terms in invertedIndexSorted:
        fileWriterD.write(str(terms[0][0])+" "+str(terms[0][1])+"\n")
        fileWriterP.write(str(terms[1])+"\n")
    fileWriterD.close()
    fileWriterP.close()
    return True

def usage():
    print "usage: " + sys.argv[0] + " -i directory-of-documents -d dictionary-file -p postings-file"

file_i = file_d = file_p = None
try:
    opts, args = getopt.getopt(sys.argv[1:], 'i:d:p:')
except getopt.GetoptError, err:
    usage()
    sys.exit(2)
for o, a in opts:
    if o == '-i':
        file_i = a
    elif o == '-d':
        file_d = a
    elif o == '-p':
        file_p = a
    else:
        assert False, "unhandled option"
if file_i == None or file_d == None or file_p == None:
    usage()
    sys.exit(2)

main(file_i, file_d, file_p)
