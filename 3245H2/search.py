#!/usr/bin/python
import re
import nltk
import sys
import getopt

OPERATORS_DICTIONARY = {"(" : -1,")" : -1, "OR": 10, "AND" : 20, "NOT" : 30}

def convertInFixToPostFix(line):
    stack = []
    postFix = []
    for token in nltk.word_tokenize(line):
        text = str(token)
        if OPERATORS_DICTIONARY.has_key(text):
            if len(stack) == 0:
                stack.append(text)
            else:
                if text == "(":
                    stack.append(text)
                elif text == ")":
                    popped = stack.pop()
                    while(len(stack)!= 0 and ( popped != "(")):
                        postFix.append(popped)
                        popped = stack.pop()
                else:
                    popped = stack.pop()
                    while(len(stack)!= 0 and (OPERATORS_DICTIONARY[popped] >= OPERATORS_DICTIONARY[text])):
                        postFix.append(popped)
                        popped = stack.pop()
                    if not(len(stack)!= 0 and (OPERATORS_DICTIONARY[popped] >= OPERATORS_DICTIONARY[text])):
                        stack.append(popped)
                    stack.append(text) 
        else:
            postFix.append(text)  
    while len(stack)!=0:
        postFix.append(stack.pop())
    return postFix

def evaluateQuery(query, file_d, file_p):
    dictionary = {}
    dictFileReader=open(file_d,'r')
    for line in dictFileReader:
        entry = line.rstrip()
        term = nltk.word_tokenize(entry)
        dictionary[term[0]] = term [1]
    stack = []
    rs = []
    notFlag= False
    for token in query:
        if OPERATORS_DICTIONARY.has_key(token):
            if(token == "NOT"):
                notFlag = True
            elif(token == "AND"):
                arg1 = stack.pop()
                arg2 = stack.pop()
                rs = performAND(dictionary,file_p,notFlag,arg1,arg2)
                notFlag=False
            elif (token == "OR"):
                arg1 = stack.pop()
                arg2 = stack.pop()
                rs = performOR(dictionary,file_p,notFlag,arg1,arg2)
                notFlag=False
            else:
                print "do nothing"
        else:
            stack.append(token)
    return True

def performAND(dictionary,file_p,flag,arg1,arg2):
    rs = []
    
    return True

def performOR(dictionary,file_p,flag, arg1,arg2):
    rs = []
    
    return True

def main(file_d,file_p,file_q,file_o):
    queryFileReader = open(file_q,'r')
    for line in queryFileReader:
        query = convertInFixToPostFix(line)
        evaluateQuery(query,file_d,file_p)
    return True
    
def usage():
    print "usage: " + sys.argv[0] + " -d dictionary-file -p postings-file -q file-of-queries -o output-file-of-results"

file_d = file_p = file_q = file_o = None
try:
    opts, args = getopt.getopt(sys.argv[1:], 'd:p:q:o')
except getopt.GetoptError, err:
    usage()
    sys.exit(2)
for o, a in opts:
    if o == '-d':
        file_d = a
    elif o == '-p':
        file_p = a
    elif o == '-q':
        file_q = a
    elif o == '-o':
        file_o = a
    else:
        assert False, "unhandled option"
if file_d == None or file_p == None or file_q == None or file_o == None:
    usage()
    sys.exit(2)

main(file_d,file_p,file_q,file_o)
